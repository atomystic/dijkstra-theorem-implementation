export class ComputePath {

    constructor(startNode, endNode, graph) {

        this.startNode = startNode
        this.endNode = endNode
        this.graph = graph
        this.currentNode;
        this.currentLinks;

    }

    getShortestDistance() {

        this.currentNode = this.getNodeByName(this.startNode)

        // setter la valeur de la plus courte distance à 0
        this.currentNode.shortestDistanceAndNodeOrigin.distanceBetween = 0;

        while (this.currentNode.name != this.endNode && this.currentLinks != 0) {

            // mettre  à jour le statut de l'objet : l'objet est choisi
            this.currentNode.chose = true;

            // récupérer les liens fils pas encore traités  du noeuds courant
            this.currentLinks = this.currentNode.neighborNodes.filter(currentLink => !currentLink.node.chose);


            if (this.currentLinks.length === 0 && this.currentNode.name != this.endNode) {

                // choisir comme noeuds courant celui qui à la distance à l'origine la plus court
                this.setNearestPeakIfNothingToExploreAsLongAsThereAreUnUsedLink();

            } else if (this.currentLinks.length != 0) {

                this.setNewDistanceToOrigin()

                this.currentNode = this.choseSmallerNode();
            }

            console.log(this.currentNode.name)
        }

        return this.getDistanceAndPath();

    }

    getDistanceAndPath() {

        let origin = "";
        let thisNode = this.currentNode;
        let finalOrder = []

        while (thisNode != null) {

            // pusher le noeud courant dans une table
            finalOrder.push(thisNode)

            // chercher le nom de l'origine du passage sur ce noeuds
            origin = thisNode.shortestDistanceAndNodeOrigin.nodeOrigin

            // aller chercher le noeud d'origine
            thisNode = this.getNodeByName(origin)

        }

        // Parcourir le chemin dans le bon sens
        return finalOrder.reverse()

    }

    getNodeByName(name) {

        return this.graph.filter(linkedNodeInstance => linkedNodeInstance.name === name)[0];

    }

    isDistanceSmaller(linkedNodeInstance) {

        return Number(this.currentNode.shortestDistanceAndNodeOrigin.distanceBetween) + Number(linkedNodeInstance.linkedNodeDistance) <= Number(linkedNodeInstance.node.shortestDistanceAndNodeOrigin.distanceBetween)
    }

    choseSmallerNode() {

        return this.currentLinks.reduce((a, b) => { return Number(a.node.shortestDistanceAndNodeOrigin.distanceBetween) <= Number(b.node.shortestDistanceAndNodeOrigin.distanceBetween) ? a : b }).node

    }

    replaceShortestDistanceAndOrigin(linkedNodeInstance) {

        const newShortestDistance = Number(this.currentNode.shortestDistanceAndNodeOrigin.distanceBetween) + Number(linkedNodeInstance.linkedNodeDistance);

        // remplacer par la nouvelle distance la plus courte 
        linkedNodeInstance.node.shortestDistanceAndNodeOrigin.distanceBetween = newShortestDistance;

        // remplacer la nouvelle origine de cette distance
        linkedNodeInstance.node.shortestDistanceAndNodeOrigin.nodeOrigin = this.currentNode.name;


    }

    setNewDistanceToOrigin() {

        this.currentLinks.forEach(linkedNodeInstance => {

            if (this.isDistanceSmaller(linkedNodeInstance)) {

                this.replaceShortestDistanceAndOrigin(linkedNodeInstance);

            }

        })

    }

    getNearestPeakIfNothingToExplore() {

        // console.log(this.graph.reduce((a, b) => Number(a.replaceShortestDistanceAndOrigin.distanceBetween) < Number(b.replaceShortestDistanceAndOrigin.distanceBetween) ? a : b))
        return this.graph.filter(x => !x.chose).reduce((a, b) => Number(a.shortestDistanceAndNodeOrigin.distanceBetween) < Number(b.shortestDistanceAndNodeOrigin.distanceBetween) ? a : b)

    }

    setNearestPeakIfNothingToExploreAsLongAsThereAreUnUsedLink() {

        this.currentNode = this.getNearestPeakIfNothingToExplore()

        this.currentNode.chose = true;

        this.currentLinks = this.currentNode.neighborNodes.filter(currentLink => !currentLink.node.chose);

        if (this.currentLinks.length != 0) {

            this.setNewDistanceToOrigin()

            this.currentNode = this.choseSmallerNode();

        } else if (this.currentNode.name != this.endNode) {

            this.currentNode = this.setNearestPeakIfNothingToExploreAsLongAsThereAreUnUsedLink();

        }
    }
}