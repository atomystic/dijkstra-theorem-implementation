import { ComputePath } from "./computePath.js"
import NodeGraph from "./nodeGraph.js"

class Main {

    constructor() {

        // récupérer le graph à l'aide de l'api fetch
        this.getGraph()

    }

    getGraph() {

        fetch('./tableLinked.json')

        .then(async(response) => {

            this.tableLinked = await response.json();

            return this.tableLinked

        }).then(graph => {

            // inititialisation des attributs de classes dès que le json est chargé
            this.initialize(graph);

        })
    }


    initialize(graph) {

        this.tableLinked = graph;
        // noeuds de départ et noeurds d'arrivé
        this.startNode = "H"
        this.endNode = "D"

        // pour chaque noeuds, on indique les noeuds voisins et la distance
        this.tableLinked

        // tableau comportant toutes les instances de noeuds
        this.tableLinkedNodes = []

        // remplissage de la table this.tableLinkedNodes des 8 insances de noeuds 
        this.setInstanceOfNode()

        // insertion des liens pour chaque noeuds
        this.setLink();

        // retour de la table du chemin le plus court
        const shortestDistanceTable = main.getShortestDistance();

        console.log(`noeuds de départ: ${main.startNode}`)
        console.log(`noeuds d'arrivé: ${main.endNode}`)
        console.log(`chemin le plus court :`)
        console.log(shortestDistanceTable)

    }

    setInstanceOfNode = () => {

        for (const linkKey in this.tableLinked) {

            const node = new NodeGraph(linkKey)
            this.tableLinkedNodes.push(node)

        }

    }

    setLink = () => {

        this.tableLinkedNodes.forEach((tableLinkedNode) => {

            const linkedNodeStrings = this.tableLinked[tableLinkedNode.name]

            tableLinkedNode.setNodeIntoTable(linkedNodeStrings, this.tableLinkedNodes)

        })

    }

    getShortestDistance = () => {

        // instanciation de la classe permettant de calculer la plus courte distance
        let graph = new ComputePath(this.startNode, this.endNode, this.tableLinkedNodes)

        // calcul et retourne la table de la plus courte distance
        return graph.getShortestDistance();

    }

}


// instanciation de la classe main 
const main = new Main();