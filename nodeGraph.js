export default class NodeGraph {

    constructor(nodeName) {

        this.name = nodeName
        this.chose = false;
        this.neighborNodes = []
        this.shortestDistanceAndNodeOrigin = { distanceBetween: Infinity, nodeOrigin: null };

    }


    setNodeIntoTable = (linkedNodeString, tableLinkedNode) => {

        linkedNodeString.split(",").forEach(linkedNodeString => {

            const linkedNodeChar = linkedNodeString.charAt(0);

            const linkedNodeDistance = linkedNodeString.slice(1);

            const linkedNodeInstance = tableLinkedNode.filter(x => x.name === linkedNodeChar)[0];

            this.neighborNodes.push({ linkedNodeDistance, node: linkedNodeInstance })


        });

    }

}